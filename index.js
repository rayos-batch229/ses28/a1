db.rooms.insertOne(

{
    "name" : "single",
    "acommodates" : 2,
    "price" : 1000,
    "description" : "A simple room with all the basic necessities",
    "rooms_available" : 10,
    "isAvailable" : false
}



)

db.rooms.insertMany([

{
  
    "name" : "double",
    "accomodates" : 3,
    "price" : 2000,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5,
    "isAvailable" : false
},
{
  
    "name" : "queen",
    "accomodates" : 4,
    "price" : 4000,
    "description" : "A room with a queen sized bed perfect for a simple gateway",
    "rooms_available" : 15,
    "isAvailable" : false
}


])

db.rooms.find({name:"double"})

{
    "_id" : ObjectId("63bf9c500285d14d2e366495"),
    "name" : "double",
    "accomodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5.0,
    "isAvailable" : false
}

db.rooms.updateOne({rooms_available:15},{$set:{rooms_available:0}})

{
    "acknowledged" : true,
    "matchedCount" : 1.0,
    "modifiedCount" : 1.0
}

db.rooms.deleteMany({rooms_available:0})

{
    "acknowledged" : true,
    "deletedCount" : 1.0
}





















db.getCollection('rooms').find({})